# syntax=docker/dockerfile:1
FROM fedora:latest
RUN yum -y install nagios
RUN htpasswd -b /etc/nagios/passwd nagiosadmin 1234
RUN mkdir /run/php-fpm
RUN /usr/sbin/php-fpm -D
RUN /usr/sbin/httpd -k start
RUN /usr/sbin/nagios -d /etc/nagios/nagios.cfg
EXPOSE 80/tcp
VOLUME /etc/nagios

